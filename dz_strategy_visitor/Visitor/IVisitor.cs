﻿namespace dz_strategy_visitor
{
    public interface IVisitor
    {
         bool visit(SerializeToJsonStrategy strategy) ;
         bool visit(SerializeToXmlStrategy strategy) ;
    }
}