﻿namespace dz_strategy_visitor
{
    public class Square
    {
        public  Point leftTop { get; set; }
        public  Point leftBottom { get; set; }
        public  Point rightTop { get; set; }
        public  Point rightBottom { get; set; }

        public Square()
        {
            
        }
        
    }
}
