﻿using System;

namespace dz_strategy_visitor
{
    [Serializable]
    public class Point
    {
        public int x { get; set; }
        public int y { get; set; }

        public Point()
        {
            
        }

        public Point(int _x, int _y)
        {
            this.x = _x;
            this.y = _y;
        }

        public override string ToString()
        {
            return $"x:{x}; y:{y}";
        }
    }
}