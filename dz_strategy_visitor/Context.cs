﻿using System;
using System.Text.Json;

namespace dz_strategy_visitor
{
    public class Context
    {
        public void DoCircle()
        {
            Point p = new Point(10, 20);
            Circle c1 = new Circle(p, 35);

            Console.WriteLine(c1.ToString());

            ISerializeStrategy strategy = new SerializeToXmlStrategy();
            
            string xmlCircle = strategy.SerializeObj(c1);
            Circle c2 = strategy.DeserializeObj<Circle>(xmlCircle);
            Console.WriteLine(c2.ToString());
            
            strategy = new SerializeToJsonStrategy();

            string json = strategy.SerializeObj<Circle>(c2);
            Circle c3 = strategy.DeserializeObj<Circle>(json);
            Console.WriteLine(c3.ToString()); 
            
        }
    }
}