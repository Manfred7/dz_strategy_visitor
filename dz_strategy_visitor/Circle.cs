﻿using System;

namespace dz_strategy_visitor
{
    [Serializable]
    public class Circle
    {
        public Point center { get; set; }
        public int radius { get; set; }

        public Circle()
        {
                     
        }
        public Circle(Point _center, int _radius)
        {
            this.center = _center;
            this.radius = _radius;

        }
        
        public override string ToString()
        {
            return $"center.x:{center.x}; center.y:{center.y}; radius:{radius} ";
        }
        
        
    }
}