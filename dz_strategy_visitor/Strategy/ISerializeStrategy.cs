﻿using System;

namespace dz_strategy_visitor
{
    interface ISerializeStrategy
    {
        string SerializeObj<T>(T obj);
         T DeserializeObj<T>(string text);
    }
}