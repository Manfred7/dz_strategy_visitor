﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace dz_strategy_visitor
{
    public class SerializeToXmlStrategy: ISerializeStrategy
    {
        private  Stream GenerateStreamFromString(string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            MemoryStream stream = new MemoryStream(byteArray);
            stream.Position = 0;
            return stream;
        }

        public  T DeserializeObj<T>(string text)
        {
            using (var stream = GenerateStreamFromString(text))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(T));
                return (T) formatter.Deserialize(stream);
            }
        }

        public  string SerializeObj<T>(T obj)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(T));

            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
        }
    }
}