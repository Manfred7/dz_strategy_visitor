﻿using System.Text.Json;

namespace dz_strategy_visitor
{
    public class SerializeToJsonStrategy: ISerializeStrategy
    {
        public  T DeserializeObj<T>(string text)
        {
            return JsonSerializer.Deserialize<T>(text);
        }

        public  string SerializeObj<T>(T obj)
        {
            return JsonSerializer.Serialize<T>(obj);
        }
    }
}