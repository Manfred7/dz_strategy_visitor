﻿using System;
using System.IO;
using System.Text.Json;
using System.Xml.Serialization;


namespace dz_strategy_visitor
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Context c = new Context();
            c.DoCircle();
        }
    }
}